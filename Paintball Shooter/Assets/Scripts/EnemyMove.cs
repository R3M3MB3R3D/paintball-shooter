﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour 
{
	public Transform Blue;
	public GameObject Bullet;

	public float enemyRotationSpeed = 150;
	public int moveSpeed = 2;

	public int fireRate = 2;
	public float fireCoolDown = 2;

	void Start ()
	{
		GetComponent<AudioSource> ();
	}

	void Update () 
	{
		Vector3 direction = Blue.position - transform.position;
		direction.Normalize ();
		float zAngle = (Mathf.Atan2 (direction.y, direction.x) * Mathf.Rad2Deg - 90);
		Quaternion targetLocation = Quaternion.Euler (0, 0, zAngle);
		transform.rotation = Quaternion.RotateTowards (transform.rotation, targetLocation, enemyRotationSpeed * Time.deltaTime);
		transform.Translate (0, moveSpeed * Time.deltaTime, 0);

		fireCoolDown -= Time.deltaTime;
		if (fireCoolDown <= 0) 
		{
			fireCoolDown = fireRate;
			Instantiate (Bullet, new Vector2 (transform.position.x, transform.position.y), transform.rotation);
		}
	}
}