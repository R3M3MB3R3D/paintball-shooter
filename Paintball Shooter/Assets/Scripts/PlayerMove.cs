﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour 
{
	public GameObject Bullet;

	//designer editable speed and rotation values
	public float speed = 5;
	public float rotate = 250;

	private Vector2 pos;

	void Start () 
	{
		pos = gameObject.transform.position;
	}

	void Update () 
	{
		var transAmount = speed * Time.deltaTime;
		var transRotate = rotate * Time.deltaTime;

		if (Input.GetKey (KeyCode.W)) 
		{
			transform.Translate (0, transAmount, 0);
		}
		if (Input.GetKey (KeyCode.S)) 
		{
			transform.Translate (0, -transAmount, 0);
		}
		if (Input.GetKey (KeyCode.D)) 
		{
			transform.Translate (transAmount, 0, 0);
		}
		if (Input.GetKey (KeyCode.A)) 
		{
			transform.Translate (-transAmount, 0, 0);
		}
		if (Input.GetKey (KeyCode.Q)) 
		{
			transform.Rotate (0, 0, transRotate);
		}
		if (Input.GetKey (KeyCode.E)) 
		{
			transform.Rotate (0, 0, -transRotate);
		}
		if (Input.GetKeyDown (KeyCode.Space))
		{
			Instantiate (Bullet, new Vector2 (transform.position.x, transform.position.y), transform.rotation);
		}
	}
}