﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameControl : MonoBehaviour 
{
	public SceneSwitcher sceneSwitcher;

	private int blueScore = 0;
	private int redScore = 0;
	private float timer = 45;

	public Text redScoreText;
	public Text blueScoreText;
	public Text roundTimerText;

	void Start () 
	{
		redScoreText.text = "Red Team: " + redScore;
		blueScoreText.text = "Blue Team: " + blueScore;
		roundTimerText.text = "Time Left: " + timer;
		sceneSwitcher = GetComponent<SceneSwitcher> ();
	}

	void Update()
	{
		timer -= Time.deltaTime;
		roundTimerText.text = "Time Left: " + Mathf.Round(timer);
		if (timer <= 0)
		{
			sceneSwitcher.LoadCredits();
		}
	}

	public void IncrementBlueScore()
	{
		blueScore++;
		blueScoreText.text = "Blue Team: " + blueScore.ToString();
	}

	public void IncrementRedScore()
	{
		redScore++;
		redScoreText.text = "Red Team: " + redScore.ToString();
	}
}