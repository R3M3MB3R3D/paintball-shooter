﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletControl : MonoBehaviour 
{
	public GameControl gameControl;


	//designer editable bullet values
	public int timer = 2;
	public int force = 400;

	void Start () 
	{
		gameControl = GetComponent<GameControl> ();
		Destroy (gameObject, timer);
		GetComponent<Rigidbody2D> ().AddForce (transform.up * force);
	}

	void OnCollisionEnter2D(Collision2D c)
	{
		if (c.gameObject.tag.Equals ("Blue")) 
		{
			gameControl.IncrementRedScore ();
		}
		if (c.gameObject.tag.Equals ("Red")) 
		{
			gameControl.IncrementBlueScore ();
		}
	}
}