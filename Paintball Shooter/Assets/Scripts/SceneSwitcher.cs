﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour 
{
	//loading the main menu
	public void LoadMenu()
	{
		SceneManager.LoadScene (0);
	}

	//loading the actual game
	public void LoadGame()
	{
		SceneManager.LoadScene (1);
	}

	//laoding the credits
	public void LoadCredits()
	{
		SceneManager.LoadScene (2);
	}

	//exiting the game
	public void ExitGame ()
	{
		Application.Quit ();
	}
}