﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour 
{

	public static GameManager instance;
	public SceneSwitcher sceneSwitcher;

	void Awake ()
	{
		if (instance == null) 
		{
			instance = this;
			DontDestroyOnLoad (this.gameObject);
		} 
		else 
		{
			Destroy (this.gameObject);
		}
		sceneSwitcher = GetComponent<SceneSwitcher> ();
	}

	void Start () 
	{
		sceneSwitcher.LoadMenu ();
	}

	void Update () 
	{

	}
}